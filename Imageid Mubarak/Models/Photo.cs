﻿using Nokia.Graphics.Imaging;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Media;

namespace Imageid_Mubarak.Models
{
    public class Photo
    {
        private static Stream _originalImageStream;
        private static Bitmap _annotationsBitmap;

        public static readonly SolidColorBrush ForegroundBrush = new SolidColorBrush(Colors.Red);
        public static readonly SolidColorBrush BackgroundBrush = new SolidColorBrush(Colors.Blue);

        public static Stream OriginalImage
        {
            get
            {
                return _originalImageStream;
            }
            set
            {
                if (_originalImageStream != value)
                {
                    if (_originalImageStream != null)
                    {
                        _originalImageStream.Close();
                    }
                    _originalImageStream = value;
                }
            }
        }

        public static Bitmap AnnotationsBitmap
        {
            get
            {
                return _annotationsBitmap;
            }
            set
            {
                if (_annotationsBitmap != value)
                {
                    if (_annotationsBitmap != null)
                    {
                        _annotationsBitmap.Dispose();
                    }
                    _annotationsBitmap = value;
                }
            }
        }

        public static LensBlurPredefinedKernelShape KernelShape { get; set; }
        public static double KernelSize { get; set; }
        public static bool Saved { get; set; }
    }
}
