﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Navigation;
using Microsoft.Phone.Controls;
using Microsoft.Phone.Shell;
using Microsoft.Phone.Tasks;
using System.Threading;
using Imageid_Mubarak.Models;
using System.Windows.Media.Imaging;
using Nokia.Graphics.Imaging;
using Windows.Storage.Streams;
using Microsoft.Xna.Framework.Media;
using System.Windows.Input;
using System.Runtime.InteropServices.WindowsRuntime;

namespace Imageid_Mubarak.Pages
{
    public partial class EditPage : PhoneApplicationPage
{

        private bool _processing = false;
        private bool _processingPending;
        private PhotoResult _photoResult;
        private bool _manipulating;
        private bool _renderedSuccesfully;
        private string _currentTexture;
        private Semaphore _semaphore = new Semaphore(1, 1);
        private double _level = .5;
        private double _angle;
        private double _initialAngle;
        private double _scale = 1.0;
        private Point _position;
        private Point _initialPosition;

        public EditPage()
        {
            InitializeComponent();
        }

        protected override void OnNavigatedTo(NavigationEventArgs e)
        {

            base.OnNavigatedTo(e);
            if (Photo.OriginalImage == null)
            {
                NavigationService.GoBack();
            }
            else
            {
                if (_photoResult != null)
                {
                    Photo.OriginalImage = _photoResult.ChosenPhoto;
                    _photoResult = null;

                    Photo.Saved = false;
                }


                if (Photo.OriginalImage != null)
                {
                    var originalBitmap = new BitmapImage()
                    {
                        DecodePixelWidth = (int)(450.0 * Application.Current.Host.Content.ScaleFactor / 100.0),
                    };
                    Photo.OriginalImage.Position = 0;
                    originalBitmap.SetSource(Photo.OriginalImage);
                    original.Source = originalBitmap;
                }
            }

        }

        private void valueChanged(object sender, RoutedPropertyChangedEventArgs<double> e)
        {
            try
            {
                _level = slider.Value / 10;
                AttemptPreviewAsync(_currentTexture);
            }
            catch (Exception)
            {
            }
        }

        private async void AttemptPreviewAsync(string texture_location = null, bool save = false)
        {
            if (!_processing)
            {
                _processing = true;

                Photo.OriginalImage.Position = 0;
                string t1;
                if (texture_location == null)
                {
                    t1 = @"Assets\Textures\texture1.png";
                }
                else
                {
                    t1 = texture_location;
                }
                _currentTexture = t1;

                var t1_file = await Windows.ApplicationModel.Package.Current.InstalledLocation.GetFileAsync(t1);


                using (StorageFileImageSource texture = new StorageFileImageSource(t1_file))
                using (StreamImageSource source = new StreamImageSource(Photo.OriginalImage))
                using (FilterEffect fe = new FilterEffect(source))
                using (WriteableBitmapRenderer wbr = new WriteableBitmapRenderer(fe, new WriteableBitmap(400, 400)))
                using (JpegRenderer jr = new JpegRenderer(fe))
                {
                    List<IFilter> filters = new List<IFilter>();
                    BlendFilter bf = new BlendFilter();
                    bf.ForegroundSource = texture;

                    ImageProviderInfo bg_info = await source.GetInfoAsync();
                    ImageProviderInfo fg_info = await texture.GetInfoAsync();
                    double size = 1;

                    double x = Orientation.HasFlag(PageOrientation.LandscapeRight) ? 1 - _position.X : _position.X;
                    double y = Orientation.HasFlag(PageOrientation.LandscapeRight) ? 1 - _position.Y : _position.Y;
                    double dw = fg_info.ImageSize.Width / bg_info.ImageSize.Width;
                    double dh = fg_info.ImageSize.Height / bg_info.ImageSize.Height;
                    bf.Level = _level;
                    bf.TargetArea = new Windows.Foundation.Rect(new Windows.Foundation.Point(x, y), new Windows.Foundation.Size(size * _scale * dw, size * _scale * dh));

                    filters.Add(bf);
                    fe.Filters = filters;
                    WriteableBitmap buff;
                    try
                    {
                        buff = await wbr.RenderAsync();

                        original.Source = buff;

                        if (save)
                        {
                            IBuffer jpg = await jr.RenderAsync();
                            MediaLibrary lib = new MediaLibrary();
                            lib.SavePicture("ImageidMubarak_" + DateTime.Now.Ticks, jpg.AsStream());
                            savePicture.Visibility = Visibility.Collapsed;
                            MessageBox.Show("Picture saved!");
                            NavigationService.GoBack();
                        }

                    }
                    catch (Exception ex)
                    {
                        MessageBox.Show(ex.Message);
                    }
                };
                _processing = false;

            }
            else
            {
            }

        }


        private void preview_click(object sender, EventArgs e)
        {
            AttemptPreviewAsync();
        }

        private void Image_Tap_1(object sender, System.Windows.Input.GestureEventArgs e)
        {
            try
            {
                AttemptPreviewAsync(@"Assets\Badges\1.png");
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        private void Image_Tap_2(object sender, System.Windows.Input.GestureEventArgs e)
        {
            AttemptPreviewAsync(@"Assets\Badges\2.png");
        }

        private void Image_Tap_3(object sender, System.Windows.Input.GestureEventArgs e)
        {
            AttemptPreviewAsync(@"Assets\Badges\3.png");
        }

        private void Image_Tap_4(object sender, System.Windows.Input.GestureEventArgs e)
        {
            AttemptPreviewAsync(@"Assets\Badges\4.png");
        }


        private void LayoutRoot_ManipulationStarted_1(object sender, System.Windows.Input.ManipulationStartedEventArgs e)
        {
            _initialAngle = _angle;
        }

        private void LayoutRoot_ManipulationDelta_1(object sender, System.Windows.Input.ManipulationDeltaEventArgs e)
        {
            if (e.PinchManipulation != null)
            {
                // Rotate
                _angle = _initialAngle + AngleOf(e.PinchManipulation.Original) - AngleOf(e.PinchManipulation.Current);

                // Scale
                _scale *= e.PinchManipulation.DeltaScale;

                // Translate according to pinch center
                double deltaX = (e.PinchManipulation.Current.SecondaryContact.X + e.PinchManipulation.Current.PrimaryContact.X) / 2 -
                    (e.PinchManipulation.Original.SecondaryContact.X + e.PinchManipulation.Original.PrimaryContact.X) / 2;
                deltaX /= LayoutRoot.ActualWidth;

                double deltaY = (e.PinchManipulation.Current.SecondaryContact.Y + e.PinchManipulation.Current.PrimaryContact.Y) / 2 -
                    (e.PinchManipulation.Original.SecondaryContact.Y + e.PinchManipulation.Original.PrimaryContact.Y) / 2;
                deltaY /= LayoutRoot.ActualHeight;

                _position.X = _initialPosition.X + deltaX;
                _position.Y = _initialPosition.Y + deltaY;
            }
            else
            {
                // Translate
                _initialAngle = _angle;
                _position.X += e.DeltaManipulation.Translation.X / LayoutRoot.ActualWidth;
                _position.Y += e.DeltaManipulation.Translation.Y / LayoutRoot.ActualHeight;
                _initialPosition.X = _position.X;
                _initialPosition.Y = _position.Y;
            }

            e.Handled = true;

            RefreshTargetArea();
        }

        private void RefreshTargetArea()
        {
            AttemptPreviewAsync(_currentTexture);
        }

        private double AngleOf(PinchContactPoints points)
        {
            Point vec = new Point(points.SecondaryContact.X - points.PrimaryContact.X, points.SecondaryContact.Y - points.PrimaryContact.Y);

            double angle = Math.Atan2(vec.Y, vec.X);

            if (angle < 0)
            {
                angle += 2 * Math.PI;
            }

            return angle * 180 / Math.PI;
        }

        private async void LayoutRoot_Tap_1(object sender, GestureEventArgs e)
        {
        }

        private void LayoutRoot_ManipulationCompleted_1(object sender, ManipulationCompletedEventArgs e)
        {
            double x = Orientation.HasFlag(PageOrientation.LandscapeRight) ? 1 - _position.X : _position.X;
            double y = Orientation.HasFlag(PageOrientation.LandscapeRight) ? 1 - _position.Y : _position.Y;
        }

        private void save_click(object sender, EventArgs e)
        {
            savePicture.Visibility = Visibility.Visible;
            AttemptPreviewAsync(_currentTexture, true);
        }
    }
}