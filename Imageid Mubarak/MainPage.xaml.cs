﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Navigation;
using Microsoft.Phone.Controls;
using Microsoft.Phone.Shell;
using Imageid_Mubarak.Resources;
using Microsoft.Phone.Tasks;
using Imageid_Mubarak.Models;

namespace Imageid_Mubarak
{
    public partial class MainPage : PhoneApplicationPage
    {
        private CameraCaptureTask _cctask = new CameraCaptureTask();
        private PhotoChooserTask _pctask = new PhotoChooserTask();
        private PhotoResult _pResult;

        // Constructor
        public MainPage()
        {
            InitializeComponent();
        }

        private void OnTakePictureClicked(object sender, System.Windows.Input.GestureEventArgs e)
        {
            _cctask.Show();
            _cctask.Completed += PhotoChooserTaskCompleted;
        }

        private void OnChoosePictureClicked(object sender, System.Windows.Input.GestureEventArgs e)
        {
            _pctask.Show();
            _pctask.Completed += PhotoChooserTaskCompleted;
        }

        private void PhotoChooserTaskCompleted(object sender, PhotoResult e)
        {
            if (e.TaskResult == TaskResult.OK)
            {
                _pResult = e;
            }
        }

        protected async override void OnNavigatedTo(NavigationEventArgs e)
        {
 	        base.OnNavigatedTo(e);

            if (_pResult != null)
            {
                Photo.OriginalImage = _pResult.ChosenPhoto;
                Photo.Saved = false;
                _pResult = null;
                NavigationService.Navigate(new Uri("/Pages/EditPage.xaml", UriKind.Relative));
            }
        }
    }
}