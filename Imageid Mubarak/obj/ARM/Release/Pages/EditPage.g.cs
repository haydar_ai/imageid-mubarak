﻿#pragma checksum "d:\data\Projects\Imageid Mubarak\Imageid Mubarak\Pages\EditPage.xaml" "{406ea660-64cf-4c82-b6f0-42d48172a799}" "9784A663C172AF71A98283C59740437B"
//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated by a tool.
//     Runtime Version:4.0.30319.34014
//
//     Changes to this file may cause incorrect behavior and will be lost if
//     the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

using Microsoft.Phone.Controls;
using Microsoft.Phone.Shell;
using System;
using System.Windows;
using System.Windows.Automation;
using System.Windows.Automation.Peers;
using System.Windows.Automation.Provider;
using System.Windows.Controls;
using System.Windows.Controls.Primitives;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Ink;
using System.Windows.Input;
using System.Windows.Interop;
using System.Windows.Markup;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Media.Imaging;
using System.Windows.Resources;
using System.Windows.Shapes;
using System.Windows.Threading;


namespace Imageid_Mubarak.Pages {
    
    
    public partial class EditPage : Microsoft.Phone.Controls.PhoneApplicationPage {
        
        internal System.Windows.Controls.Grid LayoutRoot;
        
        internal System.Windows.Controls.Grid ContentPanel;
        
        internal System.Windows.Controls.ProgressBar savePicture;
        
        internal System.Windows.Controls.Image original;
        
        internal System.Windows.Controls.Image texture1;
        
        internal System.Windows.Controls.Image texture2;
        
        internal System.Windows.Controls.Image texture3;
        
        internal System.Windows.Controls.Image texture4;
        
        internal System.Windows.Controls.Slider slider;
        
        internal Microsoft.Phone.Shell.ApplicationBarIconButton saveButton;
        
        private bool _contentLoaded;
        
        /// <summary>
        /// InitializeComponent
        /// </summary>
        [System.Diagnostics.DebuggerNonUserCodeAttribute()]
        public void InitializeComponent() {
            if (_contentLoaded) {
                return;
            }
            _contentLoaded = true;
            System.Windows.Application.LoadComponent(this, new System.Uri("/Imageid%20Mubarak;component/Pages/EditPage.xaml", System.UriKind.Relative));
            this.LayoutRoot = ((System.Windows.Controls.Grid)(this.FindName("LayoutRoot")));
            this.ContentPanel = ((System.Windows.Controls.Grid)(this.FindName("ContentPanel")));
            this.savePicture = ((System.Windows.Controls.ProgressBar)(this.FindName("savePicture")));
            this.original = ((System.Windows.Controls.Image)(this.FindName("original")));
            this.texture1 = ((System.Windows.Controls.Image)(this.FindName("texture1")));
            this.texture2 = ((System.Windows.Controls.Image)(this.FindName("texture2")));
            this.texture3 = ((System.Windows.Controls.Image)(this.FindName("texture3")));
            this.texture4 = ((System.Windows.Controls.Image)(this.FindName("texture4")));
            this.slider = ((System.Windows.Controls.Slider)(this.FindName("slider")));
            this.saveButton = ((Microsoft.Phone.Shell.ApplicationBarIconButton)(this.FindName("saveButton")));
        }
    }
}

